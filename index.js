"use strict";

const themeButton = document.querySelector(".theme__button");
const body = document.querySelector("body");
const paragraph = document.querySelectorAll("p");
const links = document.querySelectorAll(".link");
const navbarLinks = document.querySelectorAll(".navbar__item-link");

const paragraphArray = [...paragraph];
const linksArray = [...links];
const navbarLinksArray = [...navbarLinks];

function darkTheme() {
  linksArray.forEach((element) => {
    element.classList.add("theme__dark");
  });
  navbarLinksArray.forEach((element) => {
    element.classList.add("navbar__dark");
  });
  body.classList.add("theme__dark");
}

function defaultTheme() {
  linksArray.forEach((element) => {
    element.classList.remove("theme__dark");
  });

  navbarLinksArray.forEach((element) => {
    element.classList.remove("navbar__dark");
  });
  body.classList.remove("theme__dark");
}

themeButton.addEventListener("click", (event) => {
  event.preventDefault();
  if (localStorage.getItem("theme") === "dark") {
    localStorage.removeItem("theme");
  } else {
    localStorage.setItem("theme", "dark");
  }
  getDarkTheme();
});

function getDarkTheme() {
  if (localStorage.getItem("theme") === "dark") {
    darkTheme();
  } else {
    defaultTheme();
  }
}
getDarkTheme();
